Mysql修改root初始密码
========================

使用已有密码进行登陆mysql并进行修改密码
--------------------------------------------

使用已有密码进行登陆mysql::
    
    sudo cat /etc/mysql/debian.cnf # 显示出 user 和 password
    mysql -u xxx -p # 使用上面命令输出的user和password进行登陆

进入mysql进行修改密码::

    mysql> use mysql;
    mysql> ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '123456';
    mysql> flush privileges;
    mysql> quit;


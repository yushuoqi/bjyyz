在 Ubuntu 上，要添加新用户到 MySQL 数据库，您需要执行一系列 SQL 命令。首先，您需要登录到 MySQL 数据库服务器，然后创建一个新用户并为其分配适当的权限。以下是添加新用户到 MySQL 的步骤：

# 添加新用户到 MySQL 

## 1. 登录到 MySQL 服务器

使用以下命令以 root 用户身份登录到 MySQL：

```bash
mysql -u root -p
```

系统将要求您输入 root 用户的密码。

## 2. 创建新用户

要创建一个新用户，使用 `CREATE USER` 命令。将 `new_user` 替换为您想要的用户名，以及 `password` 替换为用户的密码。请确保使用强密码。

```sql
CREATE USER 'new_user'@'localhost' IDENTIFIED BY 'password';
```

- `'new_user'` 是新用户的用户名。
- `'localhost'` 表示该用户只能从本地主机连接到 MySQL。如果您希望用户能够从远程主机连接，请使用 `'%'` 代替 `'localhost'`。
- `'password'` 是用户的密码。

## 3. 分配权限

新用户被创建后，您需要为该用户分配适当的权限。以下是为用户授予权限的示例：

- 授予用户访问所有数据库的权限：

  ```sql
  GRANT ALL PRIVILEGES ON *.* TO 'new_user'@'localhost';
  ```

- 授予用户只读权限（SELECT）：

  ```sql
  GRANT SELECT ON *.* TO 'new_user'@'localhost';
  ```

- 授予用户特定数据库的所有权限：

  ```sql
  GRANT ALL PRIVILEGES ON database_name.* TO 'new_user'@'localhost';
  ```

- 授予用户所有数据库的特定权限：

  ```sql
  GRANT CREATE, DROP, DELETE ON *.* TO 'new_user'@'localhost';
  ```

## 4. 刷新权限

完成授权后，要使更改生效，运行以下命令：

```sql
FLUSH PRIVILEGES;
```

## 5. 退出 MySQL

退出 MySQL 命令行界面：

```sql
EXIT;
```

现在，您已经成功添加了一个新用户到 MySQL 数据库并为其分配了权限。该用户可以使用其用户名和密码连接到 MySQL 数据库服务器，并根据其权限执行操作。

# ifconfig命令

查看 Linux 系统上的网络接口的 MAC 地址信息，您可以使用 `ifconfig` 或 `ip` 命令。以下是如何使用这些命令查看 MAC 地址的方法。

## 使用 `ifconfig` 命令

1. 打开终端。

2. 在终端中输入以下命令并按回车键：

   ```bash
   ifconfig
   ```

   这将显示系统上所有网络接口的详细信息。

3. 查找您感兴趣的网络接口（通常是以 `eth` 或 `wlan` 开头的接口）并查看其 MAC 地址，通常在 `HWaddr` 字段下面。例如：

   ```bash
   eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
           inet 192.168.1.100  netmask 255.255.255.0  broadcast 192.168.1.255
           inet6 fe80::a00:27ff:fe11:22e7  prefixlen 64  scopeid 0x20<link>
           ether 08:00:27:11:22:33  txqueuelen 1000  (Ethernet)
           RX packets 59722  bytes 83164847 (83.1 MB)
           RX errors 0  dropped 0  overruns 0  frame 0
           TX packets 28357  bytes 3125397 (3.1 MB)
           TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
   ```

在上述示例中，MAC 地址是 `08:00:27:11:22:33`。


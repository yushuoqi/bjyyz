实用reStructuredText实践指南
=============================

.. attention::

    reStructedText的功能要多于Markdown，特别是表格、标注、脚注、链接、图片。

.. warning::

    reStructedText也比Markdown复杂，写无复杂格式的文章，Markdown比reStructureText更方便。


目录
-------------

.. attention::

    目录可以用 ``.. toctree::`` 直接生成；（需要使用标题格式，以便于索引）

标题
--------------

.. code-block:: text

    一级标题
    ========

    二级标题，其中不添加标题，单独四条短线或以上显示为分隔线。
    ---------

    三级标题
    ~~~~~~~~~~

    四级标题
    ^^^^^^^^^^^

    五级标题
    ++++++++++

    六级标题
    '''''''''''

段落
--------------

空行分段
~~~~~~~~~~~

第一段内容。

第二段和第一段间有一空行。


自动续行
~~~~~~~~~

    一个回车不分段，
    本行续上行。


不留白续行
~~~~~~~~~~~~

行尾转义字符 ``\\`` 让续行之间不留白。

代码块
--------

简易代码块，双冒号后缩进为代码块::

    int main(int argc, char *argv[])
    {
        std::cout << "Hello rst documents" << endl;
        return 0;
    }

增强代码块，声明语言类型实现语法加亮

.. code-block:: bash
    
  sudo apt install python3-sphinx python3-sphinx-rtd-theme


列表
---------

无序列表
~~~~~~~~~~~

* 星号、减号、加号开始列表。

  - 列表层级和缩进有关。

    + 和具体符号无关。

有序列表
~~~~~~~~~~~

1. 数字和点是一种编号方式。

    A. 大写字母编号。

        a. 小写字母编号。

2. 继续一级列表。

    (I) 大写罗马编号。

        i) 小写罗马编号。


字体
-----------

**粗体** 和 *斜体* 
 
上标： E = mc\ :sup:`2`

下标： Water: H\ :sub:`2`\ O 

两个连续反引号嵌入代码，如: ``git status`` 。


链接
-------

- 访问 `reStructedText 官方文档 <https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`_ 。
- 访问 `Google <http://google.com/>`_ 。
- 上面已定义，直接引用 google_ 链接。
- 链接地址在后面定义，如： GitHub_ 。
- 反引号括起多个单词的链接。如 `my blog`_ 。

.. _GitHub: http://github.com
.. _my blog: http://www.worldhello.net


内部跳转
----------

.. _fig1:

.. figure:: ../images/blue-cat.jpg

    内部跳转图例

上面定义的位置，可以：

- 通过 fig1_ 跳转。
- 或者 `点击这里 <#fig1>`__ 跳转。
- 或者参见 :ref:`fig1`\ 。

转到内部其他文档 :doc:`deploy`

脚注
----------

reST脚注的多种表示法：

- 脚注即可以手动分配数字 [1]_ ，也可以使用井号自动分配 [#]_ 。

- 自动分配脚注 [#label]_ 也可以用，添加标签形式 [#label]_ 多次引用。

- 还支持用星号嵌入符号式脚注，如这个 [*]_ 和 这个 [*]_ 。

- 使用单词做标识亦可 [CIT2012]_ 。

.. [1] 数字编号脚注。
.. [#] 井号自动编号。
.. [#label] 井号添加标签以便多次引用。
.. [*] 星号自动用符号做脚注标记。
.. [*] 星号自动用符号做脚注标记。
.. [CIT2012] 单词或其他规定格式。


图片
------

.. figure:: https://longzeping.github.io/img/apple-touch-icon.png
    :width: 32

    图：GitHub logo

GitHub Logo: |logo|

带链接的图片：
  |imglink|_

下图向右浮动：
   .. image:: https://longzeping.github.io/img/apple-touch-icon.png
      :align: right

.. |logo| image:: https://longzeping.github.io/img/apple-touch-icon.png
.. |imglink| image:: https://longzeping.github.io/img/apple-touch-icon.png
.. _imglink: https://github.com/

测试内部图片，前后要有空行：

.. image:: ../images/blue-cat.jpg

表格
---------

.. table:: 示例表格
    :class: classic

    +---------+--------+--------+
    | head1   | head2  | head3  |
    +=========+========+========+
    |         | cell   | cell   |
    | rowspan +--------+--------+
    |         | * colspan       |
    |         | * another line  |
    +---------+-----------------+

第二种表格方式：

=====  =====  =======
A      B      A and B
=====  =====  =======
False  False  False
True   False  False
False  True   False
True   True   True
=====  =====  =======


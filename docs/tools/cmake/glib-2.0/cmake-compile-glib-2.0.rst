CMake编译Glib2.0
====================

CMakeLists.txt 内容
--------------------

CMakeLists.txt 需要输入以下内容::

    # The following is the minimum required version of the compiler that will be installed
    cmake_minimum_required (VERSION 3.0)

    project(km LANGUAGES CXX)

    set(CMAKE_INCLUDE_CURRENT_DIR ON)

    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)

    set(CMAKE_BUILD_TYPE "Debug")
    set(CMAKE_CXX_FLAGS_DEBUG "$ENV{CXXFLAGS} -O0 -g2 -ggdb")
    set(CMAKE_CXX_FLAGS_RELEASE "$ENV{CXXFLAGS} -O0 -Wall")


    # Add support for `libglib2.0-dev` for all platforms 
    find_package(PkgConfig)
    pkg_search_module(GLIB REQUIRED glib-2.0)
    MESSAGE(STATUS "glib-2.0 dirs:" ${GLIB_INCLUDE_DIRS})
    MESSAGE(STATUS "glib-2.0 lib:" ${GLIB_LIBRARIES})

    # Add include directories for GLib
    include_directories(${GLIB_INCLUDE_DIRS})
    link_directories(${GLIB_LIBRARY_DIRS})

    # Need to add the executable to the list of executable paths for the command
    add_executable(km 
        # To do

        main.cpp
        )

    # Need add some libraries to the library list
    target_link_libraries(km

        # To do

        ${GLIB_LIBRARIES})


测试代码
-----------

新建 ``main.cpp`` 测试代码如下::

    #include <locale.h>
    #include <stdlib.h>
    #include <string.h>
    #include <unistd.h>
    #include <stdio.h>
    #include <glib.h>
    #include <glib/gi18n.h>
    #include <gio/gio.h>

    #include <iostream>

    void testGlib()
    {
        GList *list = nullptr;
        list = g_list_append(list, (gpointer)"Hello world!");
        list = g_list_append(list, (gpointer)"made by pcat");
        list = g_list_append(list, (gpointer)"http://pcat.cnblogs.com");
        
        if (list != nullptr) {
            std::cout << "The first item is " << static_cast<const char*>(list->data) << std::endl;
            g_list_free(list);
        } else {
            std::cout << "The list is empty." << std::endl;
        }

    }

    int main() 
    {
        testGlib();
    
        std::cout << "Hello Km project!" << std::endl;

        return 0;
    }

    
# git命令修改指定的commit提交记录并提交到远程服务器

## 1. 交互模式进行 rebase 操作。

使我们进入到一个可编辑的界面。在编辑区顶部，会列出这次 rebase 操作所涉及的所有 commit 提交记录的摘要，它们每一行默认都是以 pick 开头的。

```bash
git  rebase  -i  HEAD^^  

#或者跳到指定commitid
git rebase -i commitid^
```

## 2. 指定修改的commit

找到修改的那个 commit，将行首的 pick 关键字修改为 **edit** 。然后保存并退出。
这么做可以在执行和指定 commit 有关的 rebase 操作时暂停下来让我们对该 commit 的信息进行修改。

## 3. 修改commit

对上一个步骤指定的 commit 信息进行修改。这一步骤的界面就是填写 commit 信息的界面，更改信息后别忘记保存。

```
git  commit  --amend 
```

如果你在完成这一步骤后使用 git log 命令查看本地的代码提交记录，会发现看不到 Commit 的信息。这是因为当前仍处于 rebase 的过程中，是正常现象。

## 4. 完成剩余的 rebase 修改操作。

```bash
git  rebase  --continue 
```

## 5. 查看修改情况

确认已修改的内容和预期一致。

```bash
git log 
```

## 6. 提交到远程服务器

```bash
git push
```
 
